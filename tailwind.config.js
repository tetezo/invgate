/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      fontFamily:{
        "dm-sans": ['"DM Sans"', 'sans-serif'],
        "poppins": ['"Poppins"', 'sans-serif']
      },
      colors:{
        "light-blue": "#E4F2FF",
        "cta-primary": "#08A1FF",
        "t-primary-600": "#347DC1",
        "t-neutral-700": "#273A57",
        "t-neutral-500": "#687382",
        "t-primary-700": "#256BA5",
        "t-indigo-700": "#3538CD",
        "t-pink-700": "#C11574",
        "t-neutral-900": "#121A26",
        "t-neutral-500": "#687382",
        "t-blue-light-700": "#026AA2",
        "t-success-700": "#027A48",
        "t-blue-gray-700": "#363F72",
        "t-orange-700": "#C4320A",
        "orange-50": "#FFF6ED",
        "blue-gray-50": "#F8F9FC",
        "success-50": "#ECFDF3",
        "blue-light-50": "#F0F9FF",
        "primary-50": "#F5F9FC",
        "indigo-50": "#EEF4FF",
        "pink-50": "#FDF2FA",
        "neutral-300": "#D1D5DB",
      },
      boxShadow:{
        "b-shadow": "0px 8px 8px -4px rgba(28, 68, 117, 0.03), 0px 20px 24px -4px rgba(28, 68, 117, 0.08)"
      },
      backgroundImage:{
        "hero-video": "url('/src/assets/2_hero.webm')",
        "post-1": "url('/src/assets/5_post-1.png')",
        "post-2": "url('/src/assets/6_post-2.png')",
        "post-3": "url('/src/assets/7_post-3.png')",
        "card-1": "url('/src/assets/3_card-1.jpeg')",
        "card-2": "url('/src/assets/4_card-2.jpeg')",
        "card-3": "url('/src/assets/5_card-3.jpeg')",
      }
    },
  },
  plugins: [],
}

