const posts = [
    {
        data: 'Olivian Rhye • 20 Jan 2022', 
        title: 'UX review presentations',
        text: 'How do you create compelling presentations that wow your colleagues and impress your managers?', 
        tags: 'Research, Presentation, Design',
        img: 'bg-post-1',
        section: 'Recent blog posts'  
    },
    {
        data: 'Phoenix Baker • 19 Jan 2022', 
        title: 'Migrating to Linear 101',
        text: 'Linear helps streamline software projects, sprints, task, and bug tracking. Here´s how to get...', 
        tags: 'Research, Design',
        img: 'bg-post-2',
        section: 'Recent blog posts'   
    },
    {
        data: 'Lana Steiner • 18 Jan 2022', 
        title: 'Building your API Stack',
        text: 'The rise of RESTful APIs has been met by a rise in tools for creating, testing, and manag...', 
        tags: 'Research, Design',
        img: 'bg-post-3',
        section: 'Recent blog posts'   
    },
    {
        data: 'Alec Whitten • 17 Jan 2022', 
        title: 'Bill Walsh leadership lessons',
        text: 'Like to know the secrets of transforming a 2-14 team into a 3x Super Bowl winning Dynasty?', 
        tags: 'Leadership, Management',
        img: 'bg-card-1',
        section: 'Hear Ticket Volume, our podcast of ITSM world'   
    },
    {
        data: 'Demi WIlkinson • 16 Jan 2022', 
        title: 'PM mental models',
        text: 'Mental models are simple expressions of complex processes or relationships.', 
        tags: 'Research, Product, Frameworks',
        img: 'bg-card-2',
        section: 'Hear Ticket Volume, our podcast of ITSM world'   
    },
    {
        data: 'Candice Wu • 15 Jan 2022', 
        title: 'What is Wireframing?',
        text: "Introduction to Wireframing and it's Principles. Learn from the best in the industry.", 
        tags: 'Research, Design',
        img: 'bg-card-3',
        section: 'Hear Ticket Volume, our podcast of ITSM world'   
    }, 
]
$("#menuIcon").click(function(){
    $("#menu-collapsed").removeClass('hidden');
    $("#menuIcon").addClass('hidden');
    $("#closeMenu").removeClass('hidden').removeClass('sm:hidden').removeClass('md:hidden');
});

$("#closeMenu").click(function(){
    $("#menu-collapsed").addClass('hidden');
    $("#menuIcon").removeClass('hidden');
    $("#closeMenu").addClass('hidden').addClass('sm:hidden').addClass('md:hidden');
})

$("#button-search").click(function(){
    search($("#search-bar").val().toLowerCase());
    $("#search-view").css('display', 'block');
})

$("#search-bar-modal").on('input', function(){
    search($("#search-bar-modal").val().toLowerCase());
})

$("#closeSearch").click(function(){
    $("#search-view").css('display', 'none');
})

function search(searchParam) {
    var param = searchParam.toLowerCase();
    const filteredPosts = $.grep(posts, function(post) {
        const lowerTitle = post.title.toLowerCase();
        const lowerContent = post.text.toLowerCase();
        const lowerTags = post.tags.toLowerCase();
        const lowerSection = post.section.toLowerCase();
        return lowerTitle.includes(param) || lowerContent.includes(param) || lowerTags.includes(param) || lowerSection.includes(param);
    });
    $("#modal-content").empty();
    for (let count = 0; count < filteredPosts.length; count += 2) {
        
        const row =  $('<div>', {
            class: 'flex flex-col md:flex-row gap-x-9'
        })
        const column1 = $('<div>', {
            class: 'w-full md:w-1/2 flex flex-row gap-x-6 mb-6'
        })
        const columnImg1 = $('<div>', {
            class: 'w-[40%] ' + filteredPosts[count].img + ' bg-no-repeat bg-cover bg-center rounded-[10px]'
        })
        const columnText1 = $('<div>',{
            class: 'w-[60%] flex flex-col'
        })
        const h61 = $('<h6>', {
            text: filteredPosts[count].data,
            class: 'text-sm font-medium text-t-primary-700 pb-3'
        })
        const title1 = $('<h3>', {
            text: filteredPosts[count].title,
            class: 'text-lg font-medium text-t-neutral-900 pb-2'
        })
        const p1 = $('<p>', {
            text: filteredPosts[count].text,
            class: 'text-base font-normal text-t-neutral-500'
        })
        columnText1.append(h61).append(title1).append(p1);
        column1.append(columnImg1).append(columnText1);
        row.append(column1);

        if(filteredPosts[count + 1]){
            const column2 = $('<div>', {
                class: 'w-full md:w-1/2 flex flex-row gap-x-6 mt-0 mb-6'
            })
            const columnImg2 = $('<div>', {
                class: 'w-[40%] ' + filteredPosts[count + 1].img + ' bg-no-repeat bg-cover bg-center rounded-[10px]'
            })
            const columnText2 = $('<div>',{
                class: 'w-[60%] flex flex-col'
            })
            const h62 = $('<h6>', {
                text: filteredPosts[count + 1].data,
                class: 'text-t-primary-700'
            })
            const title2 = $('<h3>', {
                text: filteredPosts[count + 1].title,
                class: 'text-lg font-medium text-t-neutral-900 pb-2'
            })
            const p2 = $('<p>', {
                text: filteredPosts[count + 1].text,
                class: 'text-base font-normal text-t-neutral-500'
            })
            columnText2.append(h62).append(title2).append(p2);
            column2.append(columnImg2).append(columnText2);
            row.append(column2);
            
        }
        $("#modal-content").append(row);
    }
    return filteredPosts;
}

$(document).ready(function() {
    $("a[data-search-param]").click(function(event) {
      event.preventDefault();
  
      const searchParam = $(this).data("search-param");
  
      search(searchParam);
    });
  });

